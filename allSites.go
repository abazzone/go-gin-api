package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx"
)

//Message ...
type Message struct {
	ID      string `json:"id"`
	Code    string `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
}

//Sites
type AllSites struct {
	Sites []Site
}

type Site struct {
	ID   int32  `json:"asset_id"`
	Name string `json:"asset_label"`
	Code string `json:"instance_number"`
}

var RenderJSON = func(structs []interface{}, c *gin.Context, msg Message) {
	sliceStructs := make([]interface{}, len(structs))
	for i := range structs {
		sliceStructs[i] = structs[i]
	}

	c.JSON(http.StatusOK, gin.H{"data": sliceStructs, "message": msg, "next": "", "previous": "", "lastModifiedDate": ""})
}

func main() {
	var (
		apiConfig = connect()

		msg Message
	)
	router := gin.Default()

	router.GET("/api/sites", func(c *gin.Context) {
		rows, err := apiConfig.DB.Query(`SELECT 
		ms.asset_id AS mining_site_id, 
		TRIM(ms.site_name), 
		TRIM(ms.site_code) 
		from conformed_view.list_mining_sites_pdm() ms order by ms.site_name;`)
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()

		sites := make([]Site, 0)

		// if !rows.Next() {
		// 	c.JSON(http.StatusOK, gin.H{"data": "", "message": "No results", "next": "", "previous": "", "lastModifiedDate": ""})
		// }

		for rows.Next() {
			var site Site
			err := rows.Scan(&site.ID, &site.Name, &site.Code)
			if err != nil {
				log.Fatal(err)
			}
			sites = append(sites, site)
		}

		if err := rows.Err(); err != nil {
			log.Fatal(err)
		}

		// connection.SetHeaders(c)

		b := make([]interface{}, len(sites))
		for i := range sites {
			b[i] = sites[i]
		}

		RenderJSON(b, c, msg)

		// c.JSON(http.StatusOK, gin.H{"data": sites, "message": msg, "next": "", "previous": "", "lastModifiedDate": ""})
	})

	var err error

	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(router.Run(":8888"))
}

type Api struct {
	DB *pgx.Conn
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig
	// config.Host = "masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com"
	// config.User = "bdfreader"
	// config.Password = "Bdf1Reader2?!*"
	// config.Database = "abx_masterdata"
	// log.Println("\n COFIG\n ", pgx.ParseConnectionString(config))

	// var result = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset-120180711183426850100000005.cegmvtordp0k.us-west-2.rds.amazonaws.com\",\"password\":\"AxsArRcna*x6vV2Bw\",\"port\":\"5432\",\"username\":\"bdfprocessor\"}"

	var result = "{\"database\":\"postgres\",\"dbClusterIdentifier\":\"\",\"engine\":\"aurora-postgresql\",\"host\":\"192.168.99.100\",\"password\":\"docker\",\"port\":\"5432\",\"username\":\"postgres\"}"

	// log.Println(result)

	text := []byte(result)
	var dat map[string]interface{}
	if err := json.Unmarshal(text, &dat); err != nil {
		panic(err)
	}

	log.Println(dat)

	for key, val := range dat {
		switch key {
		case "host":
			config.Host = val.(string)
		case "port":
			{
				var val1 = val.(string)
				val2, err := strconv.Atoi(val1)
				if err != nil {
					panic(err)
				}
				config.Port = uint16(val2)
			}
		case "database":
			config.Database = val.(string)
		case "username":
			config.User = val.(string)
		case "password":
			config.Password = val.(string)
		}
	}

	log.Println("\n COFIG:\n ", config)

	return config
}

func (apiConfig *Api) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func connect() *Api {
	apiConfig := new(Api)
	apiConfig.InitDB()
	return apiConfig
}
