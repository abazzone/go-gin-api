package main

import (
	"testing"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
)

type Test struct {
	request events.APIGatewayProxyRequest
	expect  string
	err     error
}

func TestHandlerResult(t *testing.T) {
	errorJSON := `{
		\"data\": [],
		\"lastModifiedDate\": \"\",
		\"message\": {
		  \"id\": \"\",
		  \"code\": 200,
		  \"name\": \"\",
		  \"details\": \"\",
		  \"type\": \"\"
		},
		\"next\": \"\",
		\"previous\": \"\"
	  }`

	test := Test{
		request: events.APIGatewayProxyRequest{
			HTTPMethod: "GET",
			Body:       errorJSON,
		},
		expect: `{
			\"data\": [],
			\"lastModifiedDate\": \"\",
			\"message\": {
			  \"id\": \"\",
			  \"code\": 200,
			  \"name\": \"\",
			  \"details\": \"\",
			  \"type\": \"\"
			},
			\"next\": \"\",
			\"previous\": \"\"
		  }`,
		err: nil,
	}
	response, err := Handler(test.request)
	assert.IsType(t, test.err, err)
	assert.Equal(t, test.expect, response.Body)
}
