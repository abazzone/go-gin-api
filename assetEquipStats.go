package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"go-gin-api/internal/dbconnect"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx"
	log "github.com/sirupsen/logrus"
)

//AssetEquipStatus - details of equipment for an Asset
type AssetEquipStatus struct {
	AssetID          int        `json:"asset_id"`
	AssetStatus      string     `json:"asset_status"`
	StatusChangeStr  string     `json:"status_changed_date"`
	StatusChangeDate RecordDate `json:"-"`
	InsideInterval   bool       `json:"is_inside_interval"`
}

//RecordDate - Date type from results later formatted to ChangeStatusStr string for JSON
type RecordDate struct {
	Date time.Time
}

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
	Count   int    `json:"count"`
}

var sqlQuery = `select
						asset_status,
						status_changed_date,
						is_inside_interval
						from reporting.get_asset_status_pdm($1, $2, $3)
						order by status_changed_date  DESC`

func formatError(bodyStr string, code int) {
	log.Println(bodyStr)
	log.Fatal()
}

func validateParam(c *gin.Context) (string, error) {
	var id = c.Param("asset_id")

	if id == "" {
		return "", errors.New("missing asset_id parameter in request")
	}
	//Test if param contains only integers
	_, err := strconv.Atoi(id)
	if err != nil {
		return "", errors.New("invalid param type: must be an integer")
	}
	return id, nil
}

func getDates(interval string, unit string) (string, string, error) {

	inval, err := strconv.Atoi(interval)
	if err != nil {
		return "", "", err
	}

	var (
		timeUnit        time.Duration
		paramErrMsg     string
		paramErr        = false
		rangeErrMsg     = "interval_value out of range: the maximum value is "
		firstRecordDate = 1970
		dayUnit         = 1
	)

	endDate := time.Now()
	date70 := time.Date(firstRecordDate, endDate.Month(), endDate.Day(), endDate.Hour(), endDate.Minute(), 0, 0, time.UTC)
	maxDays := int(endDate.Sub(date70).Hours() / 24)
	maxHours := int(endDate.Sub(date70).Hours())
	maxMins := 999999

	// log.Println(maxDays, " | ", maxHours, " | ", maxMins, " | ", date70)

	switch unit {
	case "hour":
		if inval > maxHours {
			paramErrMsg = fmt.Sprintf("%s%d%s", rangeErrMsg, maxHours, " hours")
			paramErr = true
		}
		timeUnit = time.Hour
	case "day":
		if inval > maxDays {
			paramErrMsg = fmt.Sprintf("%s%d%s", rangeErrMsg, maxDays, " days")
			paramErr = true
		}
		timeUnit = time.Hour
		dayUnit = 24
	case "minute":
		if inval > maxMins {
			paramErrMsg = fmt.Sprintf("%s%d%s", rangeErrMsg, maxMins, " minutes")
			paramErr = true
		}
		timeUnit = time.Minute
	}

	if paramErr {
		formatError(paramErrMsg, 400)
	}

	var duration = timeUnit //When other unit values are requested, make this a switch

	startDate := endDate.Add(-(duration * time.Duration(inval*dayUnit)))
	if startDate.Year() < 1970 {
		var errMsg = "error in request paramameters: interval_value and interval_unit calculate a start date before 1970 - try a smaller value and/or unit"
		formatError(errMsg, 400)
	}

	log.Println("\n", startDate.Format("2006-01-02 15:04:05"), "\n", endDate.Format("2006-01-02 15:04:05"))
	log.Println("\n", dayUnit)

	return startDate.Format("2006-01-02 15:04:05"), endDate.Format("2006-01-02 15:04:05"), nil
}

func validateQueryString(qStr map[string]string) (string, string, error) {
	interval := qStr["interval_value"]
	unit := qStr["interval_unit"]
	if interval == "" || unit == "" {
		errs := errors.New("one or both of the following parameters are missing from the request: interval_value, interval_unit")
		return "", "", errs
	}

	unitType := make(map[string]bool)
	unitType["day"] = true
	unitType["hour"] = true
	unitType["minute"] = true

	_, err := strconv.Atoi(interval)
	if err != nil {
		errs := errors.New(`invalid param type: "interval_value" must only contain digits`)
		return "", "", errs
	}
	unit = strings.ToLower(unit)
	if !unitType[unit] {
		errs := errors.New(`invalid param type: "interval_unit" can only have a value of day, hour or minute`)
		return "", "", errs
	}

	return interval, unit, nil
}

func requestData(pathParam string, queryParams ...string) (pgx.Rows, error) {
	dbconnect.SecretKey = "maintenance-aurora"
	apiConfig := connect()

	start, end, err := getDates(queryParams[0], queryParams[1])
	if err != nil {
		return pgx.Rows{}, err
	}

	rows, err := apiConfig.DB.Query(sqlQuery, pathParam, start, end)
	if err != nil {
		return pgx.Rows{}, err
	}

	defer rows.Close()
	return *rows, nil
}

func processRows(id string, rows pgx.Rows, c *gin.Context) (AssetEquipStatus, Message, int, error) {
	var (
		dateFormat = "2006-01-02 15:04:05"
		msg        Message
		assetStat  AssetEquipStatus
		statDate   RecordDate
	)
	count := 0

	intID, err := strconv.Atoi(id) //convert id param to int for JSON format
	if err != nil {
		log.Fatal(err)
	}

	if rows.Next() {
		assetStat.AssetID = intID

		err := rows.Scan(&assetStat.AssetStatus, &statDate.Date, &assetStat.InsideInterval)
		assetStat.StatusChangeStr = statDate.Date.Format(dateFormat)

		if err != nil {
			log.Fatal(err)
		}
		count++
	}

	return assetStat, msg, count, nil
}

func renderJSON(c *gin.Context, assetStat AssetEquipStatus, totalCt int, messages ...Message) {
	var (
		msgs   []Message
		newMsg Message
	)

	if len(messages) != 0 {
		newMsg.Details = messages[0].Details
	}
	msgs = append(msgs, newMsg)

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	if totalCt == 0 {
		c.JSON(http.StatusOK, gin.H{"data": []AssetEquipStatus{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": "", "totalCount": totalCt})
	} else {
		c.JSON(http.StatusOK, gin.H{"data": assetStat, "message": msgs, "next": "", "previous": "", "lastModifiedDate": "", "totalCount": totalCt})
	}
}

func main() {

	router := gin.Default()

	router.GET("/v1/sites/:asset_id/assetequipstatus", func(c *gin.Context) {
		var msg Message

		id, err := validateParam(c)
		if err != nil {
			log.Panic(err.Error())
		}

		value := c.Query("interval_value")
		unit := c.Query("interval_unit")
		if value == "" || unit == "" {
			errors.New("one or both of the following parameters are missing from the request: interval_value, interval_unit")
			log.Panic(err.Error())
		}

		rows, err := requestData(id, value, unit)
		if err != nil {
			log.Panic(err.Error())
		}

		equipStatus, msg, total, err := processRows(id, rows, c)
		if err != nil {
			log.Panic(err.Error())
		}
		if total == 0 {
			msg.Details = "no Results bucko. "
		}

		renderJSON(c, equipStatus, total, msg)
	})

	var err error

	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(router.Run(":8888"))
}

//******************************************************************************************************************************************************************************************************************************

type Api struct {
	DB *pgx.Conn
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig

	var result = "{\"username\":\"maintenance\",\"password\":\"sdaczqrefsdR112easr\",\"engine\":\"postgres\",\"host\":\"rds-maintenance20180516190445375000000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"port\":\"5432\",\"dbClusterIdentifier\":\"rds-maintenance20180516190445375000000004\"}"

	text := []byte(result)
	var dat map[string]interface{}
	if err := json.Unmarshal(text, &dat); err != nil {
		panic(err)
	}

	for key, val := range dat {
		switch key {
		case "host":
			config.Host = val.(string)
		case "port":
			{
				var val1 = val.(string)
				val2, err := strconv.Atoi(val1)
				if err != nil {
					panic(err)
				}
				config.Port = uint16(val2)
			}
		case "database":
			config.Database = val.(string)
		case "username":
			config.User = val.(string)
		case "password":
			config.Password = val.(string)
		}
	}

	return config
}

func (apiConfig *Api) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func connect() *Api {
	apiConfig := new(Api)
	apiConfig.InitDB()
	return apiConfig
}
