package main

import (
	"strings"

	"github.com/facette/natsort"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

//AssetGroup root
type AssetGroup struct {
	SiteCode   string      `json:"instance_number"`
	SiteName   string      `json:"asset_label"`
	GroupNames []GroupName `json:"asset_group"`
}

//GroupName info
type GroupName struct {
	Name string `json:"asset_group_name"`
}

func main() {
	var (
		apiConfig = connection.Connect()
		msg       Message
	)
	router := gin.Default()

	var sqlQuery = `SELECT 
		site_code, 
		site_name, 
		asset_group_name 									
		from conformed_view.get_asset_group_by_asset_id_pdm($1) order by asset_group_name`

	router.GET("/v1/sites/:asset_id/siteassetgroups", func(c *gin.Context) {

		var id = c.Params.ByName("asset_id")

		log.Println(sqlQuery, id)

		rows, err := apiConfig.DB.Query(sqlQuery, id)

		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()

		var assetGrp AssetGroup
		grpNamesSort := make([]string, 0)
		grpNames := make([]GroupName, 0)

		rowsRead := 0

		for rows.Next() {
			var grpName GroupName
			if rowsRead != 0 {
				err := rows.Scan(nil, nil, &grpName.Name)
				if err != nil {
					log.Fatal(err)
				}
				grpName.Name = strings.Trim(grpName.Name, "'")
				grpNamesSort = append(grpNamesSort, grpName.Name)
			} else {
				err := rows.Scan(&assetGrp.SiteCode, &assetGrp.SiteName, &grpName.Name)
				if err != nil {
					log.Fatal(err)
				}
				grpName.Name = strings.Trim(grpName.Name, "'")
				grpNamesSort = append(grpNamesSort, grpName.Name)
			}
			rowsRead++
		}

		//natsort to sort grp names correctly when the names have a combination of chars and ints
		natsort.Sort(grpNamesSort)
		for i := 0; i < len(grpNamesSort); i++ {
			grpNames = append(grpNames, GroupName{Name: grpNamesSort[i]})
		}

		assetGrp.GroupNames = grpNames

		if err := rows.Err(); err != nil {
			log.Fatal(err)
		}

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
		c.JSON(http.StatusOK, gin.H{"data": assetGrp, "message": msg, "next": "", "previous": "", "lastModifiedDate": ""})
	})

	var err error

	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(router.Run(":8888"))
}
