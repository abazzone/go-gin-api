package main

import (
	"go-gin-api/connection"
	"net/http"
	"net/url"

	"github.com/facette/natsort"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

//AssetGroup root
type AssetGroup struct {
	GroupName string  `json:"asset_group_name"`
	Assets    []Asset `json:"assets"`
}

//Asset info
type Asset struct {
	AssetID        int    `json:"asset_id"`
	ParentID       int    `json:"parent_asset_id"`
	AssetLabel     string `json:"asset_label"`
	InstanceNumber string `json:"instance_number"`
	AssetTypeName  string `json:"asset_type_name"`
}

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
}

func main() {
	var (
		apiConfig = connection.Connect()
		msg       Message
	)
	router := gin.Default()

	var sqlQuery = `SELECT 
		asset_id, 
		coalesce(parent_asset_id, 0) as parent_asset_id, 
		asset_label,
		instance_number,
		asset_type_category_name								
		from conformed_view.get_assets_by_asset_group_pdm($1) order by asset_label`

	router.GET("/v1/sites/:asset_group_name/assetgroupassets", func(c *gin.Context) {
		var id = c.Param("asset_group_name") //req.PathParameters["asset_group_name"]
		if id == "" {
			log.Fatal("no ID")
			// return events.APIGatewayProxyResponse{Body: "Missing asset_group_name in request.", StatusCode: 400}, nil
		}
		id, err := url.QueryUnescape(id)
		if err != nil {
			log.Fatal("Problem unescaping")
			// return events.APIGatewayProxyResponse{
			// 	Body:       "Problem unescaping request param.",
			// 	StatusCode: 500,
			// }, nil
		}

		rows, err := apiConfig.DB.Query(sqlQuery, id)
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()

		var (
			assetGrp = AssetGroup{
				GroupName: id,
			}
			asset   Asset
			instNum string
			// msgs   []Message
			// newMsg Message
		)

		assetLabelsSort := make([]string, 0)
		assetsMap := make(map[string]Asset)
		count := 0

		for rows.Next() {
			err := rows.Scan(&asset.AssetID, &asset.ParentID, &asset.AssetLabel, &asset.InstanceNumber, &asset.AssetTypeName)
			if err != nil {
				log.Fatal(err)
			}
			instNum = asset.InstanceNumber
			assetsMap[instNum] = asset
			assetLabelsSort = append(assetLabelsSort, instNum)
			count++
		}
		if err := rows.Err(); err != nil {
			log.Fatal(err)
		}
		// log.Println("\n MAP: ", len(assetsMap))
		// log.Println("\n SORT: ", len(assetLabelsSort))

		// log.Println("\n The MAP: ", assetsMap)

		natsort.Sort(assetLabelsSort)

		//Loop through assetLabelsSort, match with assetsMap, and append to sorted Assets
		sortedAssets := []Asset{}
		var addAsset Asset
		for _, astLabel := range assetLabelsSort {
			addAsset = assetsMap[astLabel]
			sortedAssets = append(sortedAssets, addAsset)
		}

		assetGrp.Assets = sortedAssets

		if err := rows.Err(); err != nil {
			log.Fatal(err)
		}

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
		c.JSON(http.StatusOK, gin.H{"data": assetGrp, "message": msg, "next": "", "previous": "", "lastModifiedDate": "", "totalCount": count})
	})

	var err error

	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(router.Run(":8888"))
}
