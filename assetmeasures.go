package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx"
	log "github.com/sirupsen/logrus"
)

//Asset root
type Asset struct {
	AssetID     string         `json:"asset_id"`
	InstanceNum string         `json:"instance_number"`
	AvailData   []Availability `json:"availability_data"`
	UtilData    []Utilization  `json:"utilization_data"`
	MtbfData    []MTBF         `json:"mtbf_data"`
}

//Availability measures
type Availability struct {
	AvailFloat float64 `json:"availability"`
	RecordDate string  `json:"record_date"`
	Avail      string  `json:"-"`
}

//Utilization measures
type Utilization struct {
	UtilFloat  float64 `json:"utilizaton"`
	RecordDate string  `json:"record_date"`
	Util       string  `json:"-"`
}

//MTBF measures
type MTBF struct {
	MtbfFloat  float64 `json:"mtbf"`
	RecordDate string  `json:"record_date"`
	Mtbf       string  `json:"-"`
}

//RecDate used to populate RecordDate in structs
var RecDate time.Time

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
	Count   int    `json:"count"`
}

var (
	msgs   []Message
	newMsg Message
)

func formatError(bodyStr string, code int) {
	log.Fatal(bodyStr, code)
}

func validateParam(c *gin.Context) (string, error) {
	var id = c.Param("asset_id")

	if id == "" {
		return "", errors.New("missing asset_id parameter in request")
	}
	//Test if param contains only integers
	_, err := strconv.Atoi(id)
	if err != nil {
		return "", errors.New("invalid param type: must be an integer")
	}
	return id, nil
}

func getDates(interval string, unit string) (string, string, error) {
	inval, err := strconv.Atoi(interval)
	if err != nil {
		return "", "", err
	}

	var (
		timeUnit        time.Duration
		paramErrMsg     string
		paramErr        = false
		rangeErrMsg     = "interval_value out of range: the maximum value is "
		firstRecordYear = 1970
		dayUnit         = 1
	)

	endDate := time.Now()
	date70 := time.Date(firstRecordYear, endDate.Month(), endDate.Day(), endDate.Hour(), endDate.Minute(), 0, 0, time.UTC)
	maxDays := int(endDate.Sub(date70).Hours() / 24)
	maxHours := int(endDate.Sub(date70).Hours())
	maxMins := 999999

	log.Println("unit", unit)
	switch unit {
	case "hour":
		if inval > maxHours {
			paramErrMsg = fmt.Sprintf("%s%d%s", rangeErrMsg, maxHours, " hours")
			paramErr = true
		}
		timeUnit = time.Hour
	case "day":
		if inval > maxDays {
			paramErrMsg = fmt.Sprintf("%s%d%s", rangeErrMsg, maxDays, " days")
			paramErr = true
		}
		log.Println("unit", unit)
		timeUnit = time.Hour
		dayUnit = 24
	case "minute":
		if inval > maxMins {
			paramErrMsg = fmt.Sprintf("%s%d%s", rangeErrMsg, maxMins, " minutes")
			paramErr = true
		}
		timeUnit = time.Minute
	}

	if paramErr {
		err := errors.New(paramErrMsg)
		return "", "", err
	}

	startDate := endDate.Add(-(timeUnit * time.Duration(inval*dayUnit)))
	// log.Println("inval ----<", inval)
	// log.Println("inval*dayUnit-->", inval*dayUnit)
	// log.Println("timeUnit * time.Duration(inval*dayUnit)-->", timeUnit*time.Duration(inval*dayUnit))

	// log.Println("startDate++ ", startDate.Format("2006-01-02 15:04:05"))

	if startDate.Year() < 1970 {
		err = errors.New("error in request paramameters: interval_value and interval_unit calculate a start date before 1970 - try a smaller value and/or unit")
		return "", "", err
	}

	// log.Println("\n", startDate.Format("2006-01-02 15:04:05"), "\n", endDate.Format("2006-01-02 15:04:05"))

	return startDate.Format("2006-01-02 15:04:05"), endDate.Format("2006-01-02 15:04:05"), nil
}

func validateQueryString(qStr map[string]string) (string, string, error) {
	interval := qStr["interval_value"]
	unit := qStr["interval_unit"]
	if interval == "" || unit == "" {
		errs := errors.New("one or both of the following parameters are missing from the request: interval_value, interval_unit")
		return "", "", errs
	}

	unitType := make(map[string]bool)
	unitType["day"] = true
	unitType["hour"] = true
	unitType["minute"] = true

	_, err := strconv.Atoi(interval)
	if err != nil {
		errs := errors.New(`invalid param type: "interval_value" must only contain digits`)
		return "", "", errs
	}
	unit = strings.ToLower(unit)
	if !unitType[unit] {
		errs := errors.New(`invalid param type: "interval_unit" can only have a value of day, hour or minute`)
		return "", "", errs
	}

	return interval, unit, nil
}

func requestData(pathParam string, queryParams ...string) pgx.Rows {
	apiConfig := connect()

	var (
		sqlQuery = `select
					instance_number,
					record_datetime_gmt,
					availability_percent,
					utilization_percent,
					coalesce(mean_time_between_failure, '0') as mean_time_between_failure
					from reporting.get_asset_measures_pdm($1, $2, $3) ORDER BY record_datetime_gmt DESC`
	)
	log.Println("------------>", queryParams)
	start, end, err := getDates(queryParams[0], queryParams[1])
	if err != nil {
		return pgx.Rows{}
	}

	rows, err := apiConfig.DB.Query(sqlQuery, pathParam, start, end)
	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	// log.Println(rows)

	return *rows
}

func getFloatVals(fltVals ...string) ([]float64, error) {
	var (
		floats []float64
	)

	for i := 0; i < len(fltVals); i++ {
		fltVal, err := strconv.ParseFloat(fltVals[i], 64)
		if err != nil {
			return floats, err
		}
		floats = append(floats, fltVal)
	}
	return floats, nil
}

func processRows(id string, rows pgx.Rows, c *gin.Context) (Asset, int, error) {
	var (
		asset      = Asset{AssetID: id}
		avs        []Availability
		uts        []Utilization
		mts        []MTBF
		dateFormat = "2006-01-02 15:04"
		recDate    time.Time
	)

	count := 0
	for rows.Next() {
		var (
			ast Asset
			av  Availability
			ut  Utilization
			mt  MTBF
		)

		err := rows.Scan(&ast.InstanceNum, &recDate, &av.Avail, &ut.Util, &mt.Mtbf)
		if err != nil {
			return asset, count, err
		}

		fltVals, err := getFloatVals(av.Avail, ut.Util, mt.Mtbf)
		if err != nil {
			return asset, count, err
		}
		av1 := Availability{fltVals[0], recDate.Format(dateFormat), ""}
		ut1 := Utilization{fltVals[1], recDate.Format(dateFormat), ""}
		mt1 := MTBF{fltVals[2], recDate.Format(dateFormat), ""}

		avs = append(avs, av1)
		uts = append(uts, ut1)
		mts = append(mts, mt1)

		if count == 0 {
			asset.InstanceNum = ast.InstanceNum
		}
		count++
	}
	if rows.Err() != nil {
		return asset, count, rows.Err()
	}

	asset.AvailData = avs
	asset.UtilData = uts
	asset.MtbfData = mts

	return asset, count, nil
}

func renderJSON(c *gin.Context, ast Asset, totalCt int, messages ...Message) {
	if len(messages) != 0 {
		newMsg.Details = messages[0].Details
	}
	msgs = append(msgs, newMsg)

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	if totalCt == 0 {
		c.JSON(http.StatusOK, gin.H{"data": []Asset{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": "", "totalCount": totalCt})
	} else {
		c.JSON(http.StatusOK, gin.H{"data": ast, "message": msgs, "next": "", "previous": "", "lastModifiedDate": "", "totalCount": totalCt})
	}
}

func main() {
	router := gin.Default()

	router.GET("/v1/sites/:asset_id/assetmeasures", func(c *gin.Context) {
		var msg Message
		id, err := validateParam(c)
		if err != nil {
			formatError(err.Error(), 400)
		}

		interval := c.Query("interval_value")
		unit := c.Query("interval_unit")
		if interval == "" || unit == "" {
			errors.New("one or both of the following parameters are missing from the request: interval_value, interval_unit")
			formatError(err.Error(), 400)
		}

		var rows = requestData(id, interval, unit)

		asset, count, err := processRows(id, rows, c)
		if err != nil {
			log.Panic(err.Error())
		}
		if count == 0 {
			msg.Details = "no Results bucko. "
		}

		renderJSON(c, asset, count, msg)
	})

	log.Fatal(router.Run(":8888"))
}

//******************************************************************************************************************************************************************************************************************************

type Api struct {
	DB *pgx.Conn
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig

	var result = "{\"username\":\"maintenance\",\"password\":\"sdaczqrefsdR112easr\",\"engine\":\"postgres\",\"host\":\"rds-maintenance20180516190445375000000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"port\":\"5432\",\"dbClusterIdentifier\":\"rds-maintenance20180516190445375000000004\"}"

	text := []byte(result)
	var dat map[string]interface{}
	if err := json.Unmarshal(text, &dat); err != nil {
		panic(err)
	}

	for key, val := range dat {
		switch key {
		case "host":
			config.Host = val.(string)
		case "port":
			{
				var val1 = val.(string)
				val2, err := strconv.Atoi(val1)
				if err != nil {
					panic(err)
				}
				config.Port = uint16(val2)
			}
		case "database":
			config.Database = val.(string)
		case "username":
			config.User = val.(string)
		case "password":
			config.Password = val.(string)
		}
	}

	return config
}

func (apiConfig *Api) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func connect() *Api {
	apiConfig := new(Api)
	apiConfig.InitDB()
	return apiConfig
}
