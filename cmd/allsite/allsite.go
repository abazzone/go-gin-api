package main

import (
	"log"

	"gitlab.com/barrick/barrickasset/internal/dbconnect"
	"gitlab.com/barrick/barrickasset/internal/utils"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

//Site - describes a mining site
type Site struct {
	ID   int32  `json:"asset_id"`
	Name string `json:"asset_label"`
	Code string `json:"instance_number"`
}

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int16  `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
}

var (
	detailMsg        string
	formatError      = utils.FormatError
	renderOkResponse = utils.RenderOkResponse
)

//Handler get the requested data
func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	apiConfig, err := dbconnect.Connect()
	if err != nil {
		return formatError("Problem connecting to database", 500)
	}

	var queryString = `SELECT 
		ms.asset_id AS mining_site_id,
		TRIM(ms.site_name), 
		TRIM(ms.site_code) 
		from conformed_view.list_mining_sites_pdm() ms order by ms.site_name`

	rows, err := apiConfig.DB.Query(queryString)
	if err != nil {
		return formatError("ERROR: At SQL query execution", 500)
	}
	defer rows.Close()

	sites := make([]Site, 0)
	rowsRead := 0

	for rows.Next() {
		var site Site
		err := rows.Scan(&site.ID, &site.Name, &site.Code)
		if err != nil {
			return formatError("Problem in pgx.rows.scan, converting results to structs", 500)
		}
		sites = append(sites, site)
		rowsRead++
	}

	if err := rows.Err(); err != nil {
		return formatError("Problem occured when looping through DB results", 500)
	} else if rowsRead == 0 {
		detailMsg = "No results found."
	}

	sitesIntf := make([]interface{}, len(sites))
	for i := range sites {
		sitesIntf[i] = sites[i]
	}

	log.Println(sitesIntf)

	return renderOkResponse(sitesIntf, detailMsg)
}

func main() {
	lambda.Start(Handler)
}
