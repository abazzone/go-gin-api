package main

import (
	"testing"

	"gitlab.com/barrick/barrickasset/internal/dbconnect"
	"gitlab.com/barrick/barrickasset/internal/mock"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
)

var (
	JSONstring string
	responses  = mock.GetAllSiteResponses()
)

type aTest struct {
	request   events.APIGatewayProxyRequest
	expect    string
	expectInt int
	err       error
	data      string
}

func TestHandler(t *testing.T) {
	req := events.APIGatewayProxyRequest{Body: ""}

	mock.SecretString = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Bdf1Reader2?!*\",\"port\":\"5432\",\"username\":\"bdfreader\"}"
	dbconnect.RetrieveSecretVal = mock.MockGetSecret

	tests := []aTest{
		{
			//Test if response contains string
			request: req,
			expect:  "Cort ez",
			err:     nil,
			data:    responses["Response1"],
		}, {
			//Test if response contains string
			request: req,
			expect:  "Golden Sunlight",
			err:     nil,
			data:    responses["response1"],
		}, {
			//Test if response contains string
			request: req,
			expect:  "200",
			err:     nil,
			data:    responses["response2"],
		},
	}

	for _, test := range tests {
		JSONstring = test.data
		response, err := Handler(req)
		assert.IsType(t, test.err, err)
		assert.Contains(t, response.Body, test.expect)
	}
}

func TestErrorFormat(t *testing.T) {
	var str = "Unable to marshal JSON"
	statCode := 500

	test := aTest{
		expectInt: 500,
		err:       nil,
	}

	response, err := formatError(str, statCode)
	assert.IsType(t, test.err, err)
	assert.Equal(t, response.StatusCode, test.expectInt)
}

func TestRenderJSONsuccess(t *testing.T) {
	sites := []Site{}
	sitesIntf := make([]interface{}, 1)
	sitesIntf[0] = sites

	var msg = "No results found."

	test := aTest{
		expect: "[]",
		err:    nil,
	}

	response, err := renderOkResponse(sitesIntf, msg)
	assert.IsType(t, test.err, err)
	assert.Contains(t, response.Body, test.expect)
}
