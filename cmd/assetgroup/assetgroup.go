package main

import (
	"encoding/json"

	"gitlab.com/barrick/barrickasset/internal/dbconnect"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/facette/natsort"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

//AssetGroup - describes an asset group of specific mining site
type AssetGroup struct {
	SiteCode   string      `json:"instance_number"`
	SiteName   string      `json:"asset_label"`
	GroupNames []GroupName `json:"asset_group"`
}

//GroupName - name of a group contained within the Asset Group
type GroupName struct {
	Name string `json:"asset_group_name"`
}

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
}

var (
	msgs   []Message
	newMsg Message

	//FormatError - Create JSON for error response
	FormatError = func(bodyStr string, code int) events.APIGatewayProxyResponse {
		return events.APIGatewayProxyResponse{
			Body:       bodyStr,
			StatusCode: code,
		}
	}

	//RenderJSON - create JSON for success response
	RenderJSON = func(assetGrp AssetGroup, msg Message) (events.APIGatewayProxyResponse, error) {
		msgs = append(msgs, newMsg)

		body, err := json.Marshal(gin.H{"data": assetGrp, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
		if err != nil {
			return FormatError("Unable to marshal JSON", 500), nil
		}
		return events.APIGatewayProxyResponse{
			Body:       string(body),
			StatusCode: 200,
			Headers:    map[string]string{"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": "true"},
		}, nil
	}
)

//Handler - defines the lamda to retrieve the requested data
func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var id = req.PathParameters["asset_id"]
	if id == "" {
		return FormatError("Missing asset_id in request.", 400), nil
	}

	apiConfig, err := dbconnect.Connect()
	if err != nil {
		return FormatError("Problem connecting to database", 500), nil
	}

	var sqlQuery = `SELECT 
		TRIM(site_code), 
		TRIM(site_name), 
		TRIM(asset_group_name) 									
		from conformed_view.get_asset_group_by_site_pdm($1) order by asset_group_name`

	rows, err := apiConfig.DB.Query(sqlQuery, id)
	if err != nil {
		return FormatError("ERROR: At SQL query execution", 500), nil
	}
	defer rows.Close()

	var assetGrp AssetGroup

	grpNamesSort := make([]string, 0)
	grpNames := make([]GroupName, 0)
	rowsRead := 0

	for rows.Next() {
		var grpName GroupName
		if rowsRead != 0 {
			err := rows.Scan(nil, nil, &grpName.Name)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			err := rows.Scan(&assetGrp.SiteCode, &assetGrp.SiteName, &grpName.Name)
			if err != nil {
				return FormatError("Problem parsing results to structs", 404), nil
			}
		}
		grpNamesSort = append(grpNamesSort, grpName.Name)
		rowsRead++
	}
	if err := rows.Err(); err != nil {
		return FormatError("Problem looping through results.", 404), nil
	} else if rowsRead == 0 {
		newMsg.Code = 200
		newMsg.Details = "No results found for asset with id " + id + "."
		msgs = append(msgs, newMsg)
		body, nil := json.Marshal(gin.H{"data": []AssetGroup{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
		if err != nil {
			return FormatError("Unable to marshal JSON", 500), nil
		}
		return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200}, nil
	}

	//natsort used to sort grp names correctly when the names have a combination of chars and ints
	natsort.Sort(grpNamesSort)
	for i := 0; i < len(grpNamesSort); i++ {
		grpNames = append(grpNames, GroupName{Name: grpNamesSort[i]})
	}

	assetGrp.GroupNames = grpNames
	newMsg.Code = 200
	return RenderJSON(assetGrp, newMsg)
}

//main for lambda start
func main() {
	lambda.Start(Handler)
}
