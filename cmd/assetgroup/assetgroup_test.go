package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/barrick/barrickasset/internal/dbconnect"
	"gitlab.com/barrick/barrickasset/internal/mock"

	"github.com/aws/aws-lambda-go/events"
)

var (
	// ErrNameNotProvided is thrown when a name is not provided
	ErrNameNotProvided = errors.New("no name was provided in the HTTP body")
	JSONstring         string
)

type Test struct {
	request   events.APIGatewayProxyRequest
	expect    string
	expectInt int
	err       error
	data      string
}

func TestHandler(t *testing.T) {
	mock.SecretString = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Bdf1Reader2?!*\",\"port\":\"5432\",\"username\":\"bdfreader\"}"
	dbconnect.RetrieveSecretVal = mock.MockGetSecret

	tests := []Test{
		{
			request: events.APIGatewayProxyRequest{HTTPMethod: "GET", PathParameters: map[string]string{"asset_id": ""}},
			expect:  "Missing asset_id in request.",
			err:     nil,
		}, {
			request: events.APIGatewayProxyRequest{HTTPMethod: "GET", PathParameters: map[string]string{"asset_id": "test"}},
			expect:  "Problem looping through results.",
			err:     nil,
			data:    mock.Response1,
		},
	}
	for _, test := range tests {
		JSONstring = test.data
		response, err := Handler(test.request)
		assert.IsType(t, test.err, err)
		assert.Equal(t, test.expect, response.Body)
	}
}
