package main

import (
	"encoding/json"
	"net/url"

	"gitlab.com/barrick/barrickasset/internal/dbconnect"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/gin-gonic/gin"
)

//AssetGroup struct - root with requested group name
type AssetGroup struct {
	AssetGroupName string           `json:"asset_group_name"`
	CompGroups     []ComponentGroup `json:"asset_groups"`
}

//ComponentGroup struct
type ComponentGroup struct {
	Name       string      `json:"asset_group_name"`
	Components []Component `json:"components"`
}

//Component strcture
type Component struct {
	Name          string         `json:"component,omitempty"`
	SubComponents []SubComponent `json:"sub_components"`
}

//SubComponent strcture
type SubComponent struct {
	Name string `json:"sub_component,omitempty"`
}

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
}

//Handler get the requested data
func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var id = req.PathParameters["asset_group_name"]
	if id == "" {
		return events.APIGatewayProxyResponse{Body: "Missing asset_group_name in request.", StatusCode: 400}, nil
	}
	id, err := url.QueryUnescape(id)
	if err != nil {
		return events.APIGatewayProxyResponse{
			Body:       "Problem unescaping request param.",
			StatusCode: 500,
		}, nil
	}

	apiConfig, err := dbconnect.Connect()
	if err != nil {
		return events.APIGatewayProxyResponse{
			Body:       "Problem connecting to database",
			StatusCode: 500,
		}, nil
	}

	var sqlQuery = `select
		TRIM(ed.asset_group_name),
		TRIM(ed.component),
		TRIM(ed.sub_component)
		from conformed_view.get_equipment_detail_pdm($1) ed order by asset_group_name`

	rows, err := apiConfig.DB.Query(sqlQuery, id)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "cannot connect to db", StatusCode: 404}, nil
	}
	defer rows.Close()

	var (
		assetGrp = AssetGroup{
			AssetGroupName: id,
		}
		msgs   []Message
		newMsg Message
	)
	assetGroups := make([]ComponentGroup, 0)
	components := make([]Component, 0)
	rowsRead := 0

	for rows.Next() {
		var (
			compGrp   ComponentGroup
			comp      Component
			subComp   SubComponent
			newGrp    = true
			newCmp    = true
			grpIndex  = 0
			compIndex = 0
		)
		err := rows.Scan(&compGrp.Name, &comp.Name, &subComp.Name)

		if err != nil {
			return events.APIGatewayProxyResponse{Body: "components not found", StatusCode: 404}, nil
		}

		if len(assetGroups) != 0 {
			for index := range assetGroups {
				if comp.Name == assetGroups[index].Components[0].Name {
					newGrp = false
					grpIndex = index
					for index2 := range components {
						if comp.Name == components[index2].Name {
							newCmp = false
							compIndex = index2
							break
						}
					}
					break
				}
			}
		}

		if !newGrp {
			if !newCmp {
				components[compIndex].SubComponents = append(components[compIndex].SubComponents, subComp)
				assetGroups[grpIndex].Components = components

			}
		} else {
			components = components[:0]
			comp.SubComponents = append(comp.SubComponents, subComp)
			compGrp.Components = append(compGrp.Components, comp)
			assetGroups = append(assetGroups, compGrp)

			components = compGrp.Components
		}
		rowsRead++
	}

	if err := rows.Err(); err != nil {
		return events.APIGatewayProxyResponse{
			Body:       "Problem parsing results to structs",
			StatusCode: 500,
		}, nil
	} else if rowsRead == 0 {
		newMsg.Code = 200
		newMsg.Details = "No results found for asset group name " + id + "."
		msgs = append(msgs, newMsg)
		body, err := json.Marshal(gin.H{"data": []Component{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
		if err != nil {
			return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
		} else {
			return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200}, nil
		}
	}

	assetGrp.CompGroups = assetGroups

	newMsg.Code = 200
	msgs = append(msgs, newMsg)
	body, err := json.Marshal(gin.H{"data": assetGrp, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})

	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
	}

	return events.APIGatewayProxyResponse{
		Body:       string(body),
		StatusCode: 200,
		Headers:    map[string]string{"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": "true"},
	}, nil
}

//main for lambda start
func main() {
	lambda.Start(Handler)
}
