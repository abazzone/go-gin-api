package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/barrick/barrickasset/internal/dbconnect"
	"gitlab.com/barrick/barrickasset/internal/mock"
)

var (
	// ErrNameNotProvided is thrown when a name is not provided
	ErrNameNotProvided = errors.New("no name was provided in the HTTP body")
	JSONstring         string
)

type Test struct {
	request   events.APIGatewayProxyRequest
	expect    string
	expectInt int
	err       error
	data      string
}

var (
	secString = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Bdf1Reader2?!*\",\"port\":\"5432\",\"username\":\"bdfreader\"}"

	mockGetSecret = mock.MockGetSecret
)

func TestHandler(t *testing.T) {
	mock.SecretString = secString
	dbconnect.RetrieveSecretVal = mockGetSecret

	tests := []Test{
		{
			request: events.APIGatewayProxyRequest{HTTPMethod: "GET", PathParameters: map[string]string{"asset_group_name": ""}},
			expect:  "Missing asset_group_name in request.",
			err:     nil,
		},
	}
	for _, test := range tests {
		JSONstring = test.data
		response, err := Handler(test.request)
		assert.IsType(t, test.err, err)
		assert.Equal(t, test.expect, response.Body)
	}
}

func TestNoResults(t *testing.T) {
	mock.SecretString = secString
	dbconnect.RetrieveSecretVal = mockGetSecret

	test := Test{
		request: events.APIGatewayProxyRequest{HTTPMethod: "GET", PathParameters: map[string]string{"asset_group_name": "test"}},
		expect:  "No results",
		err:     nil,
		data:    mock.Response1,
	}

	JSONstring = test.data
	response, err := Handler(test.request)
	assert.IsType(t, test.err, err)
	assert.Contains(t, response.Body, test.expect)
}

func TestErrorFormat(t *testing.T) {
	var str = "Unable to marshal JSON"
	statCode := 500

	test := Test{
		expectInt: 500,
	}

	response := FormatError(str, statCode)
	assert.Equal(t, response.StatusCode, test.expectInt)
}
