package main

import (
	"log"

	"gitlab.com/barrick/barrickasset/internal/dbconnect"
	"gitlab.com/barrick/barrickasset/internal/utils"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/jackc/pgx"
)

//Detail struct
type Detail struct {
	ID                int      `json:"asset_id"`
	ParentID          int      `json:"parent_asset_id"`
	Label             string   `json:"asset_label"`
	InstanceNumber    string   `json:"instance_number"`
	HierarchyLevel    int      `json:"hierarchy_level"`
	HierarchyChildren []Detail `json:"hierarchy_children"`
}

var (
	formatError      = utils.FormatError
	renderOkResponse = utils.RenderOkResponse
	validateParam    = utils.ValidateIntParam
)

func requestData(pathParam string) (*pgx.Rows, error) {
	apiConfig, err := dbconnect.Connect()
	if err != nil {
		return nil, err
	}

	var sqlQuery = `select 
		asset_id, 
		COALESCE(' ' || parent_asset_id || ' ', '0')::int,
		instance_number, 
		asset_label, 
		hierarchy_level
		from conformed_view.get_mining_site_detail_pdm($1, 5, null) 
		order by hierarchy_level, instance_number`

	rows, err := apiConfig.DB.Query(sqlQuery, pathParam)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return rows, nil
}

func processRows(rows *pgx.Rows) ([]Detail, error) {
	var (
		resultArray []Detail
		detail      Detail
	)

	for rows.Next() {
		err := rows.Scan(&detail.ID, &detail.ParentID, &detail.InstanceNumber, &detail.Label, &detail.HierarchyLevel)
		if err != nil {
			return nil, err
		}
		if detail.HierarchyChildren == nil {
			detail.HierarchyChildren = make([]Detail, 0)
		}
		resultArray = append(resultArray, detail)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	} else if len(resultArray) == 0 {
		return []Detail{}, nil
	}

	return resultArray, nil
}

func initializeArray(levs [][]Detail) [][]Detail {
	for i := 0; i < len(levs); i++ {
		levs[i] = []Detail{}
	}
	return levs
}

// resultsToMap - creates map from results with two keys: hierarchylevel & assetID
// 	If assetID has matching parentID's from higher hierarchy levels, those Details are returned
func resultsToMap(curLevel int, recs []Detail) map[int]map[int][]Detail {
	var levelNum int
	levsMap := make(map[int]map[int][]Detail)
	log.Println("\nResults To Map-recs: \n", recs)

	log.Println("\nResults To Map-current level: \n", curLevel)

	index := 0
	for i := 0; i < len(recs); i++ {
		levelNum = recs[i].HierarchyLevel
		if levelNum != curLevel {
			curLevel = levelNum
			index++
		}

		if len(levsMap[levelNum]) != 0 {
			if len(levsMap[levelNum][recs[i].ParentID]) > 0 {
				levsMap[levelNum][recs[i].ParentID] = append(levsMap[levelNum][recs[i].ParentID], recs[i])
				continue
			}
			levsMap[levelNum][recs[i].ParentID] = []Detail{recs[i]}
			continue
		}
		log.Println("\nResults To Map-levelNum: \n", curLevel)
		log.Println("\nResults To Map-levsMap: \n", levsMap)

		levsMap[levelNum] = make(map[int][]Detail)
		levsMap[levelNum][recs[i].ParentID] = []Detail{recs[i]}
	}

	return levsMap
}

//Recursive function to get all of the HierarchyChildren added to response object
func attachDetails(levsMap map[int]map[int][]Detail, children []Detail) []Detail {
	var (
		level   int
		assetID int
	)
	for i := 0; i < len(children); i++ {
		level = children[i].HierarchyLevel
		assetID = children[i].ID

		if len(levsMap[level+1][assetID]) > 0 {
			children[i].HierarchyChildren = levsMap[level+1][assetID]
			if len(children[i].HierarchyChildren) > 0 && len(levsMap[level+2]) > 0 {
				children[i].HierarchyChildren = attachDetails(levsMap, children[i].HierarchyChildren)
			}
		}
	}
	return children
}

func createLevels(levsTree [][]Detail, results []Detail) [][]Detail {
	currentLevel := results[0].HierarchyLevel
	totalLevels := results[len(results)-1].HierarchyLevel - currentLevel + 1
	levsTree = make([][]Detail, totalLevels)
	levsTree = initializeArray(levsTree)

	log.Println("\nCreate Levels-levsTree: \n", levsTree)

	levelsMap := resultsToMap(currentLevel, results)
	log.Println("\nCreate Levels-levelsMap: \n", levelsMap)

	//Create new array of details used to render JSON response
	levsTree[0] = append(levsTree[0], results[0])

	if len(levelsMap) > 1 {
		levsTree[0][0].HierarchyChildren = attachDetails(levelsMap, levelsMap[currentLevel+1][results[0].ID])
	}
	return levsTree
}

func sendResponse(levsTree [][]Detail, msgString string) (events.APIGatewayProxyResponse, error) {
	//convert tree to interface array
	levsIntf := make([]interface{}, len(levsTree[0]))
	for i := range levsTree[0] {
		levsIntf[i] = levsTree[0][i]
	}

	return renderOkResponse(levsIntf, msgString)
}

//Handler get the requested data
func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	id, err := validateParam(req)
	if err != nil {
		return formatError(err.Error(), 400)
	}

	var (
		detailMsg  string
		levelsTree [][]Detail
	)

	rows, err := requestData(id)
	if err != nil {
		return formatError("Problem querying database.", 500)
	}

	results, err := processRows(rows)

	if err != nil {
		return formatError("Problem converting results to structs.", 500)
	} else if len(results) == 0 {
		levelsTree = make([][]Detail, 1)
		detailMsg = "No results found for asset with id " + id + "."
	} else {
		levelsTree = createLevels(levelsTree, results)
	}

	return sendResponse(levelsTree, detailMsg)

}

//main for lambda start
func main() {
	lambda.Start(Handler)
}
