package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/barrick/barrickasset/internal/dbconnect"
	"gitlab.com/barrick/barrickasset/internal/mock"

	"github.com/aws/aws-lambda-go/events"
)

var (
	// ErrNameNotProvided is thrown when a name is not provided
	ErrNameNotProvided = errors.New("no name was provided in the HTTP body")
	JSONstring         string
)

type Test struct {
	request   events.APIGatewayProxyRequest
	expect    string
	expectInt int
	err       error
	data      string
}

var (
	secString     = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Bdf1Reader2?!*\",\"port\":\"5432\",\"username\":\"bdfreader\"}"
	mockGetSecret = mock.MockGetSecret
	responses     = mock.GetSiteDetailResponses()
)

func TestHandler(t *testing.T) {
	mock.SecretString = secString
	dbconnect.RetrieveSecretVal = mockGetSecret

	tests := []Test{
		{
			request: events.APIGatewayProxyRequest{HTTPMethod: "GET", PathParameters: map[string]string{"asset_id": ""}},
			expect:  "missing asset_id parameter in request",
			err:     nil,
		}, {
			request: events.APIGatewayProxyRequest{HTTPMethod: "GET", PathParameters: map[string]string{"asset_id": "test"}},
			expect:  "invalid param type: must be an integer",
			err:     nil,
		}, {
			request: events.APIGatewayProxyRequest{HTTPMethod: "GET", PathParameters: map[string]string{"asset_id": "74483"}},
			expect:  "{\"data\":[],\"lastModifiedDate\":\"\",\"message\":[{\"id\":\"\",\"code\":200,\"name\":\"\",\"details\":\"No results found for asset with id 74483.\",\"type\":\"\"}],\"next\":\"\",\"previous\":\"\"}",
			err:     nil,
			data:    responses["Response3"],
		},
	}
	for _, test := range tests {
		JSONstring = test.data
		response, err := Handler(test.request)
		assert.IsType(t, test.err, err)
		assert.Equal(t, test.expect, response.Body)
	}
}

func TestNoResults(t *testing.T) {
	mock.SecretString = secString
	dbconnect.RetrieveSecretVal = mockGetSecret

	test := Test{
		request: events.APIGatewayProxyRequest{HTTPMethod: "GET", PathParameters: map[string]string{"asset_id": "101010"}},
		expect:  "No results found for asset with id 101010.",
		err:     nil,
		data:    responses["Response2"],
	}

	JSONstring = test.data
	response, err := Handler(test.request)
	assert.IsType(t, test.err, err)
	assert.Contains(t, response.Body, test.expect)
}

func TestErrorFormat(t *testing.T) {
	var str = "Unable to marshal JSON"
	statCode := 500

	test := Test{
		expectInt: 500,
		err:       nil,
	}

	response, err := formatError(str, statCode)
	assert.IsType(t, test.err, err)
	assert.Equal(t, response.StatusCode, test.expectInt)
}

func TestInitializeArray(t *testing.T) {
	var levsTree = make([][]Detail, 10)

	test := Test{
		expectInt: 10,
	}
	var response = initializeArray(levsTree)
	assert.Equal(t, len(response), test.expectInt)
}

func getMap() map[int]map[int][]Detail {
	resultArray := []Detail{{74483, 85118, "In Pit Material Handling", "CZ1-114", 4, []Detail{}}, {3369, 74483, "In Pit Crushing", "CZ1-1141", 5, []Detail{}}, {29486, 74483, "In Pit Conveying", "CZ1-1142", 5, []Detail{}}}

	return resultsToMap(4, resultArray)
}

func TestResultsToMap(t *testing.T) {
	test := Test{
		expectInt: 2,
	}

	var response = getMap()
	r := len(response[5][74483])

	assert.Equal(t, r, test.expectInt)
}

func TestAttachDetails(t *testing.T) {
	var (
		m = getMap()
		c = m[5][74483]
	)

	test := Test{
		expect: "In Pit Crushing",
	}

	var response = attachDetails(m, c)
	assert.Equal(t, response[0].Label, test.expect)
}

func TestCreateLevels(t *testing.T) {
	resultArray := []Detail{{31720, 90310, "Convertidor De Torque", "VE1-1123-TRH390-DVTN-CONV", 4, []Detail{}}, {91195, 31720, "Convertidor Cat 793c", "793CV019", 5, []Detail{}}, {23895, 31720, "Lineas De Convertidor", "VE1-1123-TRH390-DVTN-CONV-LNCN", 5, []Detail{}}, {71393, 31720, "Valvula Luck Up", "VE1-1123-TRH390-DVTN-CONV-VLUP", 5, []Detail{}}, {63785, 31720, "Yoke De Convertidor", "VE1-1123-TRH390-DVTN-CONV-YOCN", 5, []Detail{}}}

	var testTree [][]Detail

	test := Test{
		expectInt: 2,
	}

	var response = createLevels(testTree, resultArray)
	assert.Equal(t, len(response), test.expectInt)
}

func TestSuccessResponse(t *testing.T) {
	resultArray := []Detail{{31720, 90310, "Convertidor De Torque", "VE1-1123-TRH390-DVTN-CONV", 4, []Detail{}}, {91195, 31720, "Convertidor Cat 793c", "793CV019", 5, []Detail{}}, {23895, 31720, "Lineas De Convertidor", "VE1-1123-TRH390-DVTN-CONV-LNCN", 5, []Detail{}}, {71393, 31720, "Valvula Luck Up", "VE1-1123-TRH390-DVTN-CONV-VLUP", 5, []Detail{}}, {63785, 31720, "Yoke De Convertidor", "VE1-1123-TRH390-DVTN-CONV-YOCN", 5, []Detail{}}}

	var testTree [][]Detail
	testTree = createLevels(testTree, resultArray)

	test := Test{
		expect: responses["Response3"],
		err:    nil,
	}
	response, err := sendResponse(testTree, "")
	assert.IsType(t, test.err, err)
	assert.Equal(t, response.Body, test.expect)
}
