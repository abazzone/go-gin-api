package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	// "github.com/facette/natsort"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx"
)

//Component - main struct for JSON elements
type Component struct {
	CompCode          string `json:"-"` //`json:"component"`
	SubCompCode       string `json:"-"` //`json:"sub_component"`
	RecordType        string `json:"-"`
	AssetID           int    `json:"asset_id"`
	ParentAssetID     int    `json:"parent_asset_id,omitempty"`
	AssetLabel        string `json:"asset_label"`
	InstanceNum       string `json:"instance_number"`
	ComponentLabel    string `json:"component_label,omitempty"`
	SubComponentLabel string `json:"sub_component_label,omitempty"`
	*Components
	*SubComponents
}

//SubComponents - Added to component if asset id = subcomponent parent id
type SubComponents struct {
	Subs []Component `json:"sub_components"`
}

//Components - Added to component if asset id = subcomponent parent id
type Components struct {
	Comps []Component `json:"components"`
}

func getTimeAndCount(httpStat int, count int) string {
	var modTime = time.Now().Format(http.TimeFormat)
	str := `next: "", previous: "", status: &s, lastModifiedDate: %d, totalCount: &s`
	result := fmt.Sprintf(str, modTime, count)
	return result
}

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
}

func formatError(bodyStr string, code int) { // (events.APIGatewayProxyResponse,  - return
	// return events.APIGatewayProxyResponse{
	// 	Body:       bodyStr,
	// 	StatusCode: code,
	// }, nil
	log.Panic(bodyStr, code)
}

func validateParam(c *gin.Context) (string, error) { //req events.APIGatewayProxyRequest - argument
	// var id = req.PathParameters["asset_id"]
	var id = c.Param("asset_id")

	if id == "" {
		return "", errors.New("missing asset_id parameter in request")
	}
	//Test if param contains only integers
	_, err := strconv.Atoi(id)
	if err != nil {
		return "", errors.New("invalid param type: must be an integer")
	}
	return id, nil
}

func requestData(pathParam string) (pgx.Rows, error) {

	var apiConfig = connect()

	var sqlQuery = `select
		component,
		sub_component,
		record_type,
		asset_id,
		COALESCE(' ' || parent_asset_id || ' ', '0')::int,
		asset_label,
		instance_number,
		component_label,
		sub_component_label
		from conformed_view.get_asset_components_by_asset_id_pdm($1, 2) order by record_type, instance_number`

	rows, err := apiConfig.DB.Query(sqlQuery, pathParam)
	if err != nil {
		return pgx.Rows{}, err
	}
	defer rows.Close()

	return *rows, nil
}

//Components are added to results array, sub components are put into a map
func processResults(rows pgx.Rows, c *gin.Context) ([]Component, map[int][]Component, int) {
	var (
		results  []Component
		comp     Component
		count    int
		countMap int
		countRes int
	)

	subCompMap := make(map[int][]Component)
	// count := 1 //first record already appended
	for rows.Next() {
		err := rows.Scan(&comp.CompCode, &comp.SubCompCode, &comp.RecordType, &comp.AssetID, &comp.ParentAssetID, &comp.AssetLabel, &comp.InstanceNum, &comp.ComponentLabel, &comp.SubComponentLabel)
		if err != nil {
			//return events.APIGatewayProxyResponse{Body: "components not found", StatusCode: 404}, nil
			log.Fatal(err)
		} else if comp.RecordType == "Sub-Component" {
			countMap++
			comp.ComponentLabel = ""
			subCompMap[comp.ParentAssetID] = append(subCompMap[comp.ParentAssetID], comp)
		} else {
			countRes++
			comp.SubComponentLabel = ""
			results = append(results, comp)
		}
		count++
	}
	log.Println("\nMap: ", countMap)
	log.Println("\nCountRes: ", countRes)
	log.Println("\ncount: ", count)

	return results, subCompMap, count
}

func createTree(components []Component, subCompMap map[int][]Component) []Component {
	assetID := 0
	for i := 0; i < len(components); i++ {
		assetID = components[i].AssetID
		if len(subCompMap[assetID]) != 0 {
			subComps := subCompMap[assetID]
			components[i].SubComponents = &SubComponents{subComps}
		} else {
			components[i].SubComponents = &SubComponents{[]Component{}}
		}
	}
	return components
}

func renderJSON(c *gin.Context, rootComp Component, msg Message, count int) { //(events.APIGatewayProxyResponse, error)  - response
	var messages = make([]Message, 0)
	messages = append(messages, msg)
	// if count == 1 {
	// 	rootComp.Components = []Component{}
	// }
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.JSON(http.StatusOK, gin.H{"data": rootComp, "message": messages, "next": "", "previous": "", "lastModifiedDate": "", "totalCount": count})
}

func main() {
	router := gin.Default()

	router.GET("/v1/sites/:asset_id/assetcomponents", func(c *gin.Context) {
		var (
			newMsg Message
			// messages  []Message
			rootAsset Component
			rows      pgx.Rows
			total     int
		)

		id, err := validateParam(c)
		if err != nil {
			formatError(err.Error(), 400)
		}

		rows, err = requestData(id)
		if err != nil {
			formatError(err.Error(), 400)
		}

		components, subCompMap, total := processResults(rows, c)
		if total == 0 {
			newMsg.Code = 200
			newMsg.Details = "No results found for asset with id " + id + "."

			var msgs []Message
			msgs = append(msgs, newMsg)
			c.JSON(http.StatusOK, gin.H{"data": []Component{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": "", "totalCount": total})
		}

		rootAsset = components[0]
		if len(components) == 1 {
			rootAsset.Components = &Components{[]Component{}}
			log.Println(rootAsset)
		} else {
			components = components[1:]
			childComponents := createTree(components, subCompMap)
			rootAsset.Components = &Components{childComponents}
		}

		newMsg.Code = 200

		renderJSON(c, rootAsset, newMsg, total)
	})

	var err error

	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(router.Run(":8888"))
}

//******************************************************************************************************************************************************************************************************************************
//******************************************************************************************************************************************************************************************************************************
//******************************************************************************************************************************************************************************************************************************
//******************************************************************************************************************************************************************************************************************************
//******************************************************************************************************************************************************************************************************************************

type API struct {
	DB *pgx.Conn
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig

	var result = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180425235342950900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"asset-masterdata-rds.qa.aws.barrick.com\",\"password\":\"Bdf1Reader2?!*\",\"port\":\"5432\",\"username\":\"bdfreader\"}"

	text := []byte(result)
	var dat map[string]interface{}
	if err := json.Unmarshal(text, &dat); err != nil {
		panic(err)
	}

	for key, val := range dat {
		switch key {
		case "host":
			config.Host = val.(string)
		case "port":
			{
				var val1 = val.(string)
				val2, err := strconv.Atoi(val1)
				if err != nil {
					panic(err)
				}
				config.Port = uint16(val2)
			}
		case "database":
			config.Database = val.(string)
		case "username":
			config.User = val.(string)
		case "password":
			config.Password = val.(string)
		}
	}

	return config
}

func (apiConfig *API) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func connect() *API {
	apiConfig := new(API)
	apiConfig.InitDB()
	return apiConfig
}
