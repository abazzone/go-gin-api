package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/facette/natsort"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx"
	log "github.com/sirupsen/logrus"
)

//AssetGroup root
type AssetGroup struct {
	SiteCode   string      `json:"instance_number"`
	SiteName   string      `json:"asset_label"`
	GroupNames []GroupName `json:"asset_group"`
}

//GroupName info
type GroupName struct {
	Name string `json:"asset_group_name"`
}

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
	Count   int    `json:"count"`
}

var (
	msgs   []Message
	newMsg Message
)

func formatError(bodyStr string, code int) { // (events.APIGatewayProxyResponse,  - return
	// return events.APIGatewayProxyResponse{
	// 	Body:       bodyStr,
	// 	StatusCode: code,
	// }, nil
	log.Fatal(bodyStr, code)
}

func validateParam(c *gin.Context) (string, error) { //req events.APIGatewayProxyRequest - argument
	// var id = req.PathParameters["asset_id"]
	var id = c.Param("asset_id")

	if id == "" {
		return "", errors.New("missing asset_id parameter in request")
	}
	//Test if param contains only integers
	_, err := strconv.Atoi(id)
	if err != nil {
		return "", errors.New("invalid param type: must be an integer")
	}
	return id, nil
}

func requestData(pathParam string) pgx.Rows {
	// apiConfig, err := DbConnect()
	// if err != nil {
	// 	return nil, err
	// }

	var apiConfig = connect()

	var sqlQuery = `SELECT 
		TRIM(site_code), 
		TRIM(site_name), 
		TRIM(asset_group_name) 									
		from conformed_view.get_asset_group_by_asset_id_pdm($1, null) order by asset_group_name`

	rows, err := apiConfig.DB.Query(sqlQuery, pathParam)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	return *rows
}

func processRows(id string, rows pgx.Rows, c *gin.Context) (AssetGroup, Message, error) {
	var assetGrp AssetGroup

	grpNamesSort := make([]string, 0)
	grpNames := make([]GroupName, 0)
	rowsRead := 0

	for rows.Next() {
		var grpName GroupName
		if rowsRead != 0 {
			err := rows.Scan(nil, nil, &grpName.Name)
			if err != nil {
				log.Fatal(err)
			}
			grpNamesSort = append(grpNamesSort, grpName.Name)
		} else {
			err := rows.Scan(&assetGrp.SiteCode, &assetGrp.SiteName, &grpName.Name)
			if err != nil {
				log.Fatal(err)
			}
			grpNamesSort = append(grpNamesSort, grpName.Name)
		}
		rowsRead++
	}
	newMsg.Count = rowsRead

	if err := rows.Err(); err != nil {
		// return events.APIGatewayProxyResponse{
		// 	Body:       "Problem parsing results to structs",
		// 	StatusCode: 500,
		// }, nil

		formatError("Problem parsing results to structs", 500)
		return assetGrp, newMsg, err
	} else if rowsRead == 0 {
		// newMsg.Code = 200
		// newMsg.Details = "No results found for asset with id " + id + "."
		// msgs = append(msgs, newMsg)
		//body, err := json.Marshal(gin.H{"data": []AssetGroup{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
		// if err != nil {
		// 	//return events.APIGatewayProxyResponse{Body: "Unable to marshal JSON", StatusCode: 500}, nil
		// 	formatError("Unable to marshal JSON", 500)
		// 	return assetGrp, err
		// } else {
		// return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200}, nil
		// }
		newMsg.Code = 200
		newMsg.Details = "No results found for asset_id " + id
		msgs = append(msgs, newMsg)

		c.JSON(http.StatusOK, gin.H{"data": []AssetGroup{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
		// msgs = append(msgs, newMsg)
		// assetGrp = AssetGroup{}
		// //renderJSON(c, assetGrp, msg)
		// c.JSON(http.StatusOK, gin.H{"data": []AssetGroup{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
		err = errors.New(newMsg.Details)
		return assetGrp, Message{}, err
	} else {

		//natsort to sort grp names correctly when the names have a combination of chars and ints
		natsort.Sort(grpNamesSort)
		for i := 0; i < len(grpNamesSort); i++ {
			grpNames = append(grpNames, GroupName{Name: grpNamesSort[i]})
		}

		assetGrp.GroupNames = grpNames
	}
	return assetGrp, newMsg, nil
}

func renderJSON(c *gin.Context, assetGrp AssetGroup, messages ...Message) { //(events.APIGatewayProxyResponse, error)  - response
	// if len(assetGrp.GroupNames) == 0 {
	// 	//assetGrp.GroupNames = append(assetGrp.GroupNames, []string{})
	// 	log.Fatal()
	// }

	// body, err := json.Marshal(gin.H{"data": assetGrp, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
	// if err != nil {
	// 	// return events.APIGatewayProxyResponse{
	// 	// 	Body:       "Unable to marshal JSON",
	// 	// 	StatusCode: 500,
	// 	// }, nil
	// 	formatError("Unable to marshal JSON", 500)
	// }

	if len(messages) != 0 {
		newMsg.Details = messages[0].Details
	}
	newMsg.Code = 200
	msgs = append(msgs, newMsg)

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.JSON(http.StatusOK, gin.H{"data": assetGrp, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})

	// return events.APIGatewayProxyResponse{
	// 	Body:       string(body),
	// 	StatusCode: 200,
	// 	Headers:    map[string]string{"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": "true"},
	// }, nil
}

func main() {
	router := gin.Default()

	router.GET("/v1/sites/:asset_id/assetcomponents", func(c *gin.Context) {
		id, err := validateParam(c)
		if err != nil {
			formatError(err.Error(), 400)
		}

		var rows = requestData(id)

		assetGroup, msg, err := processRows(id, rows, c)
		if err != nil {
			log.Panic(err.Error())
		}

		renderJSON(c, assetGroup, msg)
	})

	log.Fatal(router.Run(":8888"))
}

//******************************************************************************************************************************************************************************************************************************

type Api struct {
	DB *pgx.Conn
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig

	var result = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Bdf1Processor2?!*\",\"port\":\"5432\",\"username\":\"bdfprocessor\"}"

	text := []byte(result)
	var dat map[string]interface{}
	if err := json.Unmarshal(text, &dat); err != nil {
		panic(err)
	}

	for key, val := range dat {
		switch key {
		case "host":
			config.Host = val.(string)
		case "port":
			{
				var val1 = val.(string)
				val2, err := strconv.Atoi(val1)
				if err != nil {
					panic(err)
				}
				config.Port = uint16(val2)
			}
		case "database":
			config.Database = val.(string)
		case "username":
			config.User = val.(string)
		case "password":
			config.Password = val.(string)
		}
	}

	return config
}

func (apiConfig *Api) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func connect() *Api {
	apiConfig := new(Api)
	apiConfig.InitDB()
	return apiConfig
}
