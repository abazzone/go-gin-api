package connection

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx"
)

type Api struct {
	DB *pgx.Conn
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig
	config.Host = "masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com"
	config.User = "bdfreader"
	config.Password = "Bdf1Reader2?!*"
	config.Database = "abx_masterdata"

	return config
}

func (apiConfig *Api) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func SetHeaders(con *gin.Context) {
	con.Header("Access-Control-Allow-Origin", "*")
	con.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
}

func Connect() *Api {
	apiConfig := new(Api)
	apiConfig.InitDB()
	return apiConfig
}
