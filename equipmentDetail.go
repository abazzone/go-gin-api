package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strconv"

	// "github.com/facette/natsort"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx"
)

//AssetGroup struct - root with requested group name
type AssetGroup struct {
	AssetGroupName string           `json:"asset_group_name"`
	CompGroups     []ComponentGroup `json:"asset_groups"`
}

//ComponentGroup struct
type ComponentGroup struct {
	Name       string      `json:"asset_group_name"`
	Components []Component `json:"components"`
}

//Component strcture
type Component struct {
	Name          string         `json:"component,omitempty"`
	SubComponents []SubComponent `json:"sub_components"`
}

//SubComponent strcture
type SubComponent struct {
	Name string `json:"sub_component,omitempty"`
}

//Message - Contains array of messages for errors/info
type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
}

func main() {
	var apiConfig = connect()
	router := gin.Default()

	var sqlQuery = `select
		TRIM(ed.asset_group_name),
		TRIM(ed.component),
		TRIM(ed.sub_component)
		from conformed_view.get_equipment_detail_pdm($1) ed order by asset_group_name`

	router.GET("/v1/assetgroups/:asset_group_name/components", func(c *gin.Context) {
		var id = c.Param("asset_group_name")
		if id == "" {
			log.Fatal("No param")
		}
		id, err := url.QueryUnescape(id)
		if err != nil {
			log.Fatal("Query escape fail")
		}

		rows, err := apiConfig.DB.Query(sqlQuery, id)
		if err != nil {
			log.Fatal("Query fail", err)
		}
		defer rows.Close()

		var (
			assetGrp = AssetGroup{
				AssetGroupName: id,
			}
			msgs   []Message
			newMsg Message
		)
		assetGroups := make([]ComponentGroup, 0)
		components := make([]Component, 0)
		rowsRead := 0

		for rows.Next() {
			var (
				compGrp   ComponentGroup
				comp      Component
				subComp   SubComponent
				newGrp    = true
				newCmp    = true
				grpIndex  = 0
				compIndex = 0
			)
			err := rows.Scan(&compGrp.Name, &comp.Name, &subComp.Name)
			if err != nil {
				log.Fatal("Row scan fail", err)
			}

			if len(assetGroups) != 0 {
				for index := range assetGroups {
					if comp.Name == assetGroups[index].Components[0].Name {
						newGrp = false
						grpIndex = index
						for index2 := range components {
							if comp.Name == components[index2].Name {
								newCmp = false
								compIndex = index2
								break
							}
						}
						break
					}
				}
			}

			if !newGrp {
				if !newCmp {
					components[compIndex].SubComponents = append(components[compIndex].SubComponents, subComp)
					assetGroups[grpIndex].Components = components
				}
			} else {
				components = components[:0]
				comp.SubComponents = append(comp.SubComponents, subComp)
				compGrp.Components = append(compGrp.Components, comp)
				assetGroups = append(assetGroups, compGrp)

				components = compGrp.Components
			}
			rowsRead++
		}
		// log.Println("ROWS READ: ", rowsRead)
		// log.Println("COMPONENTS READ: ", rowsRead)

		if err := rows.Err(); err != nil {
			log.Fatal(err)
		} else if rowsRead == 0 {
			newMsg.Code = 200
			newMsg.Details = "No results found for asset group name " + id + "."
			msgs = append(msgs, newMsg)
			c.JSON(http.StatusOK, gin.H{"data": []Component{}, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
		}

		assetGrp.CompGroups = assetGroups

		newMsg.Code = 200
		msgs = append(msgs, newMsg)
		c.JSON(http.StatusOK, gin.H{"data": assetGrp, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
	})

	var err error

	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(router.Run(":8888"))
}

type Api struct {
	DB *pgx.Conn
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig
	// config.Host = "masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com"
	// config.User = "bdfreader"
	// config.Password = "Bdf1Reader2?!*"
	// config.Database = "abx_masterdata"
	// log.Println("\n COFIG\n ", pgx.ParseConnectionString(config))

	var result = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Bdf1Processor2?!*\",\"port\":\"5432\",\"username\":\"bdfprocessor\"}"

	log.Println(result)

	text := []byte(result)
	var dat map[string]interface{}
	if err := json.Unmarshal(text, &dat); err != nil {
		panic(err)
	}

	log.Println(dat)

	for key, val := range dat {
		switch key {
		case "host":
			config.Host = val.(string)
		case "port":
			{
				var val1 = val.(string)
				val2, err := strconv.Atoi(val1)
				if err != nil {
					panic(err)
				}
				config.Port = uint16(val2)
			}
		case "database":
			config.Database = val.(string)
		case "username":
			config.User = val.(string)
		case "password":
			config.Password = val.(string)
		}
	}

	log.Println("\n COFIG:\n ", config)

	return config
}

func (apiConfig *Api) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func connect() *Api {
	apiConfig := new(Api)
	apiConfig.InitDB()
	return apiConfig
}
