package main

import (
	"fmt"
	"log"

	"github.com/gobuffalo/packr"
)

func main() {
	box := packr.NewBox("./connection")
	data, err := box.MustString("connect.go")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(data.InitDB)

	// data, err := ioutil.ReadFile("./connection/dbconnect.txt")
	// if err != nil {
	// 	fmt.Println("File reading error", err)
	// 	return
	// }
	// fmt.Println("Contents of file:", string(data))
	// var dbconnect = data
	// fmt.Println(dbconnect.InitDB)
}
