package dbconnect

import (
	"encoding/json"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/jackc/pgx"
	log "github.com/sirupsen/logrus"
)

//PgxRows - Exporting to be used instead of files needing to explicitly import pgx
var PgxRows *pgx.Rows

//API DB is the open connection if successful
type API struct {
	DB *pgx.Conn
}

//SecretKey used to look up in Secrets Manager
var SecretKey = "rds/asset/aurora-abx_masterdata-bdfprocessor"

//Connect get connection details from Secrets Manager and attempt to connect
var Connect = func() (*API, error) {
	apiConfig := new(API)
	connectSuccess, err := InitDB(apiConfig, SecretKey)
	if connectSuccess != true || err != nil {
		log.Error("DB connection did not open.")
		return nil, err
	}
	return apiConfig, nil
}

//InitDB  initial the DB connection with key
var InitDB = func(apiConfig *API, key string) (bool, error) {
	var err error
	if len(key) != 0 {
		apiConfig.DB, err = pgx.Connect(ExtractConfig(key))
		if err != nil {
			log.Fatal("Got error attempting to connect to database: '", err)
			return false, err
		}
	} else {
		log.Fatal("Missing secret key name to retrieve connection details.")
	}
	return apiConfig.DB.IsAlive(), nil
}

//ExtractConfig PostgreSQL connection
var ExtractConfig = func(secret string) pgx.ConnConfig {
	var config pgx.ConnConfig
	result, err := GetSecretValue(secret)
	if err != nil {
		log.Fatal(err)
	}

	text := []byte(result)
	var dat map[string]interface{}
	if err := json.Unmarshal(text, &dat); err != nil {
		log.Fatal(err)
	}

	for key, val := range dat {
		switch key {
		case "host":
			config.Host = val.(string)
		case "port":
			{
				var val1 = val.(string)
				val2, err := strconv.Atoi(val1)
				if err != nil {
					panic(err)
				}
				config.Port = uint16(val2)
			}
		case "database":
			config.Database = val.(string)
		case "username":
			config.User = val.(string)
		case "password":
			config.Password = val.(string)
		}
	}
	return config
}

//GetSecretValue value to take secretid and version, to return the secret string back.
var GetSecretValue = func(secretID string) (string, error) {
	var secretValue = ""
	if len(strings.TrimSpace(secretID)) == 0 {
		return "", nil
	}
	result, err := RetrieveSecretVal(secretID)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case secretsmanager.ErrCodeResourceNotFoundException:
				log.Error(secretsmanager.ErrCodeResourceNotFoundException, aerr.Error())
			case secretsmanager.ErrCodeInvalidParameterException:
				log.Error(secretsmanager.ErrCodeInvalidParameterException, aerr.Error())
			case secretsmanager.ErrCodeInvalidRequestException:
				log.Error(secretsmanager.ErrCodeInvalidRequestException, aerr.Error())
			case secretsmanager.ErrCodeDecryptionFailure:
				log.Error(secretsmanager.ErrCodeDecryptionFailure, aerr.Error())
			case secretsmanager.ErrCodeInternalServiceError:
				log.Error(secretsmanager.ErrCodeInternalServiceError, aerr.Error())
			default:
				log.Error(aerr.Error())
			}
		} else {
			log.Error(err.Error())
		}
		secretValue = "nothing found"
		return secretValue, nil
	}
	secString := *result.SecretString
	return secString, nil
}

//RetrieveSecretVal func
var RetrieveSecretVal = func(secretID string) (*secretsmanager.GetSecretValueOutput, error) {
	endpoint := "secretsmanager.us-west-2.amazonaws.com"
	region := "us-west-2"
	client, err := session.NewSession(&aws.Config{
		Endpoint: &endpoint,
		Region:   &region})
	if err != nil {
		log.Fatal(err.Error())
	}
	svc := secretsmanager.New(client)
	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secretID),
	}

	return svc.GetSecretValue(input)
}
