package dbconnect

import (
	"testing"

	"github.com/aws/aws-lambda-go/events"
	"github.com/jackc/pgx"
	"github.com/stretchr/testify/assert"
)

type Test struct {
	request events.APIGatewayProxyRequest
	expect  string
	err     error
	data    string
}

func TestSecretFound(t *testing.T) {
	RetrieveSecretVal = MockGetSecret
	SecretString = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Test*\",\"port\":\"5432\",\"username\":\"tester\"}"

	test := FuncTest{
		Expect: "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Test*\",\"port\":\"5432\",\"username\":\"tester\"}",
		Err:    nil,
	}

	response, err := GetSecretValue("test/secret")
	assert.IsType(t, test.Err, err)
	assert.Equal(t, test.Expect, response)
}

func TestSecretNotFound(t *testing.T) {
	RetrieveSecretVal = MockGetSecret

	test := FuncTest{
		Expect: "nothing found",
		Err:    nil,
	}

	response, err := GetSecretValue("fail")
	assert.IsType(t, test.Err, err)
	assert.Equal(t, test.Expect, response)
}

func TestStringSecretToConfig(t *testing.T) {
	RetrieveSecretVal = MockGetSecret

	test := FuncTest{
		Expect: "success",
		Err:    nil,
	}

	response, err := FuncMockExtract()
	assert.IsType(t, test.Err, err)
	assert.IsType(t, test.Expect, response)
}

func TestConnectError(t *testing.T) {
	InitDB = MockInitDB
	RetrieveSecretVal = MockGetSecret
	var pgErr pgx.PgError
	test := FuncTest{
		PgxError: pgErr,
	}

	_, err := Connect()
	assert.IsType(t, test.PgxError, err)
}
