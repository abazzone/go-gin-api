package mock

import (
	"errors"
	"log"

	"gitlab.com/barrick/barrickasset/internal/dbconnect"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/aws/aws-sdk-go/service/secretsmanager/secretsmanageriface"
	"github.com/jackc/pgx"
)

type Test struct {
	Request events.APIGatewayProxyRequest
	Expect  string
	Err     error
	Data    string
}

type FuncTest struct {
	Expect        string
	ExpectedError string
	Err           error
}

type MockSecretsManagerClient struct {
	secretsmanageriface.SecretsManagerAPI
}

var (
	MockConnect = pgx.Connect

	mockJSONsecret = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\", \"engine\":\"aurora-postgresql\"\"host\": \"asset-masterdata-rds.nprd.aws.barrick.com\",\"password\": \"Bdf1Reader2?!*\", \"port\": \"5432\",\"username\":\"bdfreader\"}"

	SecretString = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Test*\",\"port\":\"5432\",\"username\":\"tester\"}"
)

func (c *MockSecretsManagerClient) GetSecretValue(input *secretsmanager.GetSecretValueInput) (*secretsmanager.GetSecretValueOutput, error) {
	secret := *input.SecretId
	if secret == "fail" {
		response := &secretsmanager.GetSecretValueOutput{}
		return response, errors.New(secretsmanager.ErrCodeResourceNotFoundException)
	}

	var secretStr = &SecretString

	response := &secretsmanager.GetSecretValueOutput{
		ARN:          aws.String("arn:aws:secretsmanager:us-west-2:695603418123:secret:rds/asset/aurora-abx_masterdata-bdfreader-RWsCVr"),
		Name:         aws.String("rds/asset/aurora-abx_masterdata-bdfreader"),
		SecretString: secretStr,
		VersionId:    aws.String("26CFF36E-DF99-4807-B849-1125370449D7"),
	}

	return response, nil
}

//MockGetSecret - overwrite get secret function
var MockGetSecret = func(secretID string) (*secretsmanager.GetSecretValueOutput, error) {
	mockSvc := &MockSecretsManagerClient{}

	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secretID),
	}
	return mockSvc.GetSecretValue(input)
}

var MockIgnoreParamTest = func(req events.APIGatewayProxyRequest) (string, error) {
	var id = req.PathParameters["asset_id"]
	return id, nil
}

func GetAllSiteResponses() map[string]string {
	var respMap = map[string]string{
		"Response1": "{\"data\":[{\"asset_id\":20820,\"site_name\":\"Golden Sunlight\",\"site_code\":\"MT\"},{\"asset_id\":69118,\"site_name\":\"Pueblo Viejo\",\"site_code\":\"PV\"},{\"asset_id\":58156,\"site_name\":\"Goldstrike\",\"site_code\":\"GS\"},{\"asset_id\":40923,\"site_name\":\"Turquoise Ridge\",\"site_code\":\"TR\"},{\"asset_id\":19704,\"site_name\":\"Hemlo\",\"site_code\":\"HM\"},{\"asset_id\":45156,\"site_name\":\"Cortez\",\"site_code\":\"CZ\"}]}",

		"Response2": "{\"statusCode\":404,\"headers\":null,\"body\":\"site detail not found\"}",
	}
	return respMap
}

func GetSiteDetailResponses() map[string]string {
	var respMap = map[string]string{
		"Response1": "{\"data\":[{\"asset_id\":74483,\"parent_asset_id\":85118,\"asset_label\":\"In Pit Material Handling\",\"instance_number\":\"CZ1-114\",\"hierarchy_level\":4,\"hierarchy_children\":[{\"asset_id\":3369,\"parent_asset_id\":74483,\"asset_label\":\"In Pit Crushing\",\"instance_number\":\"CZ1-1141\",\"hierarchy_level\":5,\"hierarchy_children\":[]},{\"asset_id\":29486,\"parent_asset_id\":74483,\"asset_label\":\"In Pit Conveying\",\"instance_number\":\"CZ1-1142\",\"hierarchy_level\":5,\"hierarchy_children\":[]}]}],\"lastModifiedDate\":\"\",\"message\":[{\"id\":\"\",\"code\":200,\"name\":\"\",\"details\":\"\",\"type\":\"\"}],\"next\":\"\",\"previous\":\"\"}",

		"Response2": "{\"data\":[],\"lastModifiedDate\":\"\",\"message\":[{\"id\":\"\",\"code\":200,\"name\":\"\",\"details\":\"No results found for asset with id 101010.\",\"type\":\"\"}],\"next\":\"\",\"previous\":\"\"}",

		"Response3": "{\"data\":[{\"asset_id\":31720,\"parent_asset_id\":90310,\"asset_label\":\"Convertidor De Torque\",\"instance_number\":\"VE1-1123-TRH390-DVTN-CONV\",\"hierarchy_level\":4,\"hierarchy_children\":[{\"asset_id\":91195,\"parent_asset_id\":31720,\"asset_label\":\"Convertidor Cat 793c\",\"instance_number\":\"793CV019\",\"hierarchy_level\":5,\"hierarchy_children\":[]},{\"asset_id\":23895,\"parent_asset_id\":31720,\"asset_label\":\"Lineas De Convertidor\",\"instance_number\":\"VE1-1123-TRH390-DVTN-CONV-LNCN\",\"hierarchy_level\":5,\"hierarchy_children\":[]},{\"asset_id\":71393,\"parent_asset_id\":31720,\"asset_label\":\"Valvula Luck Up\",\"instance_number\":\"VE1-1123-TRH390-DVTN-CONV-VLUP\",\"hierarchy_level\":5,\"hierarchy_children\":[]},{\"asset_id\":63785,\"parent_asset_id\":31720,\"asset_label\":\"Yoke De Convertidor\",\"instance_number\":\"VE1-1123-TRH390-DVTN-CONV-YOCN\",\"hierarchy_level\":5,\"hierarchy_children\":[]}]}],\"lastModifiedDate\":\"\",\"message\":[{\"id\":\"\",\"code\":200,\"name\":\"\",\"details\":\"\",\"type\":\"\"}],\"next\":\"\",\"previous\":\"\"}",
	}
	return respMap
}

var MockInitDB = func(apiConfig *dbconnect.API, key string) (bool, error) {
	var config pgx.ConnConfig
	config.Host = "masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com"
	config.User = "bdfprocessor"
	config.Password = "Bdf1Processor2?!*"
	config.Database = "abx_masterdata"

	var err error
	apiConfig.DB, err = pgx.Connect(config)
	if err != nil {
		log.Fatal("Got error attempting to connect to database: '", err)
		return false, err
	}

	return apiConfig.DB.IsAlive(), nil

}
