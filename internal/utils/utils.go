package utils

import (
	"encoding/json"
	"errors"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/gin-gonic/gin"
)

type Message struct {
	ID      string `json:"id"`
	Code    int    `json:"code"`
	Name    string `json:"name"`
	Details string `json:"details"`
	Type    string `json:"type"`
}

type emptyStruct struct{}

var (
	//FormatError - create error JSON response
	FormatError = func(bodyStr string, code int) (events.APIGatewayProxyResponse, error) {
		return events.APIGatewayProxyResponse{
			Body:       bodyStr,
			StatusCode: code,
		}, nil
	}

	//RenderOkResponse - create JSON for response
	RenderOkResponse = func(structSlice []interface{}, detailMsg string) (events.APIGatewayProxyResponse, error) {
		var (
			msgs   []Message
			newMsg Message
		)
		newMsg.Code = 200

		if detailMsg != "" {
			newMsg.Details = detailMsg
		}
		msgs = append(msgs, newMsg)

		body, err := json.Marshal(gin.H{"data": structSlice, "message": msgs, "next": "", "previous": "", "lastModifiedDate": ""})
		if err != nil {
			return FormatError("Unable to marshal JSON", 500)
		}

		return events.APIGatewayProxyResponse{
			Body:       string(body),
			StatusCode: 200,
			Headers:    map[string]string{"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": "true"},
		}, nil
	}

	//ValidateIntParam - returns error if param is empty or not an integer
	ValidateIntParam = func(req events.APIGatewayProxyRequest) (string, error) {
		var id = req.PathParameters["asset_id"]
		if id == "" {
			return "", errors.New("missing asset_id parameter in request")
		}
		//Test if param contains only integers
		_, err := strconv.Atoi(id)
		if err != nil {
			return "", errors.New("invalid param type: must be an integer")
		}
		return id, nil
	}
)
