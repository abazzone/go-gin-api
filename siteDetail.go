package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx"
)

//Detail struct
type Detail struct {
	ID                int      `json:"asset_id"`
	ParentID          int      `json:"parent_asset_id"`
	Label             string   `json:"asset_label"`
	InstanceNumber    string   `json:"instance_number"`
	HierarchyLevel    int      `json:"hierarchy_level"`
	HierarchyChildren []Detail `json:"hierarchy_children"` //,omitempty"`
}

//Message ...
type Message struct {
	ID     string `json:"id"`
	Code   int    `json:"code"`
	Name   string `json:"name"`
	Detail string `json:"Detail"`
	Type   string `json:"type"`
}

func requestData(reqParam string) pgx.Rows {
	var (
		apiConfig = connect()

		sqlQuery = `select 
			asset_id, 
			COALESCE(' ' || parent_asset_id || ' ', '0')::int,
			instance_number, 
			asset_label, 
			hierarchy_level
			from conformed_view.get_mining_site_detail_pdm($1, 5, null) 
			order by hierarchy_level, instance_number`
	)

	rows, err := apiConfig.DB.Query(sqlQuery, reqParam)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	return *rows
}

func processRows(id string, rows pgx.Rows, cont *gin.Context) []Detail {

	var (
		resultArray []Detail
		msg         Message
		detail      Detail
	)

	for rows.Next() {
		err := rows.Scan(&detail.ID, &detail.ParentID, &detail.InstanceNumber, &detail.Label, &detail.HierarchyLevel)
		if err != nil {
			log.Println(detail)
			log.Fatal(err)
		}
		if detail.HierarchyChildren == nil {
			detail.HierarchyChildren = make([]Detail, 0)
		}
		resultArray = append(resultArray, detail)
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	} else if len(resultArray) == 0 {
		msg.Code = 200
		msg.Detail = "No results found for asset_id " + id
		cont.JSON(http.StatusOK, gin.H{"data": []Detail{}, "message": msg, "next": "", "previous": "", "lastModifiedDate": ""})
	}

	log.Println("ROWS: ", resultArray)
	return resultArray
}

func initializeArray(levs [][]Detail) [][]Detail {
	for i := 0; i < len(levs); i++ {
		levs[i] = []Detail{}
	}
	return levs
}

func resultsToMap(curLevel int, recs []Detail) map[int]map[int][]Detail {
	var levelNum int
	levsMap := make(map[int]map[int][]Detail)

	index := 0
	for i := 0; i < len(recs); i++ {
		levelNum = recs[i].HierarchyLevel
		if levelNum != curLevel {
			curLevel = levelNum
			index++
		}

		if len(levsMap[levelNum]) != 0 {
			if len(levsMap[levelNum][recs[i].ParentID]) > 0 {
				levsMap[levelNum][recs[i].ParentID] = append(levsMap[levelNum][recs[i].ParentID], recs[i])
				continue
			}
			levsMap[levelNum][recs[i].ParentID] = []Detail{recs[i]}
			continue
		}

		levsMap[levelNum] = make(map[int][]Detail)
		levsMap[levelNum][recs[i].ParentID] = []Detail{recs[i]}
	}

	return levsMap
}

//Recursive function to get all of the HierarchyChildren added to response object
func attachDetails(levsMap map[int]map[int][]Detail, children []Detail) []Detail {
	var (
		level   int
		assetID int
	)
	for i := 0; i < len(children); i++ {
		level = children[i].HierarchyLevel
		assetID = children[i].ID

		if len(levsMap[level+1][assetID]) > 0 {
			children[i].HierarchyChildren = levsMap[level+1][assetID]
			if len(children[i].HierarchyChildren) > 0 && len(levsMap[level+2]) > 0 {
				children[i].HierarchyChildren = attachDetails(levsMap, children[i].HierarchyChildren)
			}
		}
	}
	return children
}

var RenderJSON = func(structs []interface{}, c *gin.Context, msg Message) {
	c.JSON(http.StatusOK, gin.H{"data": structs, "message": msg, "next": "", "previous": "", "lastModifiedDate": ""})
}

func main() {

	router := gin.Default()

	router.GET("/asset/v1/sites/:asset_id", func(c *gin.Context) {
		var (
			id = c.Params.ByName("asset_id")

			rows = requestData(id)

			results = processRows(id, rows, c)

			currentLevel = results[0].HierarchyLevel
			totalLevels  = results[len(results)-1].HierarchyLevel - currentLevel + 1
			levelsTree   = make([][]Detail, totalLevels)
			msg          Message
		)

		log.Println(results)
		//levelsMap - using map to avoid looping through all results to get details.
		// 	Map has two keys, hierarchylevel & assetID, to return all detail records having matching ParentID's
		levelsMap := resultsToMap(currentLevel, results)

		//Create new array of details used to render JSON response
		levelsTree = initializeArray(levelsTree)
		levelsTree[0] = append(levelsTree[0], results[0])

		if len(levelsMap) > 1 {
			levelsTree[0][0].HierarchyChildren = attachDetails(levelsMap, levelsMap[currentLevel+1][results[0].ID])
		}

		msg.Code = 200

		b := make([]interface{}, len(levelsTree[0]))
		for i := range levelsTree[0] {
			b[i] = levelsTree[0][i]
		}
		RenderJSON(b, c, msg)

		// c.JSON(http.StatusOK, gin.H{"data": levelsTree[0], "message": msg, "next": "", "previous": "", "lastModifiedDate": ""})
	})

	log.Fatal(router.Run(":8555"))
}

type Api struct {
	DB *pgx.Conn
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig

	var result = "{\"database\":\"abx_masterdata\",\"dbClusterIdentifier\":\"masterdata-asset20180327011614905900000004\",\"engine\":\"aurora-postgresql\",\"host\":\"masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com\",\"password\":\"Bdf1Processor2?!*\",\"port\":\"5432\",\"username\":\"bdfprocessor\"}"

	text := []byte(result)
	var dat map[string]interface{}
	if err := json.Unmarshal(text, &dat); err != nil {
		panic(err)
	}

	for key, val := range dat {
		switch key {
		case "host":
			config.Host = val.(string)
		case "port":
			{
				var val1 = val.(string)
				val2, err := strconv.Atoi(val1)
				if err != nil {
					panic(err)
				}
				config.Port = uint16(val2)
			}
		case "database":
			config.Database = val.(string)
		case "username":
			config.User = val.(string)
		case "password":
			config.Password = val.(string)
		}
	}

	return config
}

func (apiConfig *Api) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func connect() *Api {
	apiConfig := new(Api)
	apiConfig.InitDB()
	return apiConfig
}
